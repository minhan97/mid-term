#pragma once
#include <string>

using namespace std;

class Category
{
private:
	wstring type;
	wstring info;
	int number;
public:
	Category();
	Category(wstring Type, wstring Info, int Num);
	~Category();

	wstring getType();
	wstring getInfo();
	int getNumber();

	void setType(wstring Type);
	void setInfo(wstring Info);
	void setNumber(int Number);
};