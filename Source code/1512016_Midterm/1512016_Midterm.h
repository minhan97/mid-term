﻿#pragma once
#include "resource.h"
#include "Category.h"
#include <vector>
#include <fstream>
#include <locale>
#include <codecvt>
#include <sstream>
#include <windowsx.h>
#include <CommCtrl.h>

#define MAX_LOADSTRING 100

using namespace std;

#pragma comment(lib, "comctl32.lib")
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

vector<wstring> defaultType = {
	L"Ăn uống",
	L"Di chuyển",
	L"Nhà cửa",
	L"Xe cộ",
	L"Nhu yếu phẩm",
	L"Dịch vụ"
};

struct Color
{
	int red = 0, green = 0, blue = 0;
	Color()
	{
	}
	Color(int r, int g, int b)
	{
		red = r;
		green = g;
		blue = b;
	}
};

struct typeCategory
{
	wstring type = L"";
	int number = 0;
	float sweepAngle = 0;
	float startAngle = 0;
	Color color;

	typeCategory()
	{
	}
};

struct Coord
{
	int x = 0;
	int y = 0;
	Coord(int x, int y)
	{
		this->x = x;
		this->y = y;
	}
};

vector<Category> g_Cat;
vector<typeCategory> typeCate;
Coord g_Origin(510, 475);
int radius = 75;
int total = 0; //total number

// Global Variables:
HINSTANCE hInst;								// current instance
HWND g_hWnd;
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

HFONT g_Font = NULL;
HWND g_Category = NULL;
HWND g_ListCategory = NULL;
HWND g_Number = NULL;
HWND g_hListCategory = NULL;
HWND g_Info = NULL;
HWND g_hNumber = NULL;
HWND g_btnAdd = NULL;
HWND g_Categories;
HWND g_pieChart = NULL;

WNDPROC wndProcCategory;
WNDPROC wndProcListview;

float getSweepAngle(int index);
void updateCoordCate();

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPTSTR lpCmdLine, _In_ int nCmdShow);
ATOM MyRegisterClass(HINSTANCE hInstance);
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow);
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

//command
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
void OnDestroy(HWND hWnd);
void OnPaint(HWND hWnd);
void OnCategoryCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
LRESULT CALLBACK categoryWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK listviewWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

//draw
void drawChart(HDC hDC);
void drawNote(HDC hDC, HFONT hFont);

//category
void addCategory(Category cat);
int getNumberofType(wstring type);
void setDefaultypeCate();

//file
vector<Category> readFile(string filename);
void saveFile(vector<Category> v, string filename);