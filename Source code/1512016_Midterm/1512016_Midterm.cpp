﻿// 1512016_Midterm.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "1512016_Midterm.h"

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPTSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MY1512016_MIDTERM, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MY1512016_MIDTERM));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MY1512016_MIDTERM));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_BTNFACE + 1);
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_MY1512016_MIDTERM);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{

	hInst = hInstance; // Store instance handle in our global variable

	g_hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		10, 10, 640, 640, NULL, NULL, hInstance, NULL);

	if (!g_hWnd)
	{
		return FALSE;
	}

	ShowWindow(g_hWnd, nCmdShow);
	UpdateWindow(g_hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	InitCommonControls();

	//read file and save data
	g_Cat = readFile("data.txt");

	//get font
	g_Font = CreateFont(16, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, VIETNAMESE_CHARSET,
		OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, TEXT("Calibri"));

	//add category control
	g_Category = CreateWindowEx(0, L"Button", L"Thêm chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
		10, 10, 600, 75, hWnd, NULL, hInst, NULL);
	SendMessage(g_Category, WM_SETFONT, WPARAM(g_Font), TRUE); //set font

	//combo box control
	g_ListCategory = CreateWindowEx(0, L"COMBOBOX", L"MyCombo", CBS_DROPDOWNLIST | WS_CHILD | WS_VISIBLE,
		27, 30, 150, 300, g_Category, NULL, hInst, NULL);
	for (int i = 0; i < defaultType.size(); ++i)
	{
		//set type for combo box
		SendMessage(g_ListCategory, CB_ADDSTRING, 0, LPARAM(defaultType.at(i).c_str()));
	}
	SendMessage(g_ListCategory, WM_SETFONT, WPARAM(g_Font), TRUE); // set font

	//description edit
	g_Info = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER,
		200, 30, 150, 22, g_Category, NULL, hInst, NULL);
	SendMessage(g_Info, EM_SETCUEBANNER, 0, LPARAM(L"Mô tả")); //set placeholder
	SendMessage(g_Info, WM_SETFONT, WPARAM(g_Font), TRUE); // set font

	//number edit
	//only input number, not others
	g_hNumber = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER,
		370, 30, 100, 22, g_Category, NULL, hInst, NULL);
	SendMessage(g_hNumber, EM_SETCUEBANNER, 0, LPARAM(L"Số tiền")); //set placeholder
	SendMessage(g_hNumber, WM_SETFONT, WPARAM(g_Font), TRUE); //set font

	//button add category
	g_btnAdd = CreateWindowEx(0, L"BUTTON", L"Thêm", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON,
		490, 30, 70, 22, g_Category, (HMENU)IDC_ADDBUTTON, hInst, NULL);
	SendMessage(g_btnAdd, WM_SETFONT, WPARAM(g_Font), TRUE); //set font

	//winproc for create category
	wndProcCategory = (WNDPROC)SetWindowLongPtr(g_Category, GWLP_WNDPROC, (LONG_PTR)&categoryWndProc);

	//add detail category control
	g_hListCategory = CreateWindowEx(0, L"Button", L"Danh sách chi tiêu", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
		10, 95, 600, 270, hWnd, NULL, hInst, NULL);
	SendMessage(g_hListCategory, WM_SETFONT, WPARAM(g_Font), TRUE); // set font

	//listview for detail category
	g_Categories = CreateWindowEx(LVS_EX_FULLROWSELECT, WC_LISTVIEW, L"",
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | LVS_REPORT | LVS_EX_GRIDLINES,
		27, 30, 550, 200, g_hListCategory, NULL, hInst, NULL);
	SendMessage(g_Categories, LVM_SETEXTENDEDLISTVIEWSTYLE, 0, LVS_EX_FULLROWSELECT);

	LVCOLUMN lvCol;

	// Category column
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvCol.cx = 150;
	lvCol.pszText = _T("Loại chi tiêu");
	ListView_InsertColumn(g_Categories, 0, &lvCol);

	// Information column
	lvCol.cx = 300;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = _T("Nội dung chi tiêu");
	ListView_InsertColumn(g_Categories, 1, &lvCol);

	// Amount column
	lvCol.cx = 100;
	lvCol.pszText = _T("Số tiền (VND)");
	lvCol.fmt = LVCFMT_RIGHT;
	ListView_InsertColumn(g_Categories, 2, &lvCol);

	for (int i = 0; i < g_Cat.size(); ++i)
	{
		LVITEM lv;

		lv.mask = LVIF_TEXT;
		lv.iItem = i;
		lv.iSubItem = 0;
		lv.pszText = new WCHAR[g_Cat[i].getType().length() + 1];
		wcscpy(lv.pszText, g_Cat[i].getType().c_str());
		ListView_InsertItem(g_Categories, &lv);

		lv.iSubItem = 1;
		lv.pszText = new WCHAR[g_Cat[i].getInfo().length() + 1];
		wcscpy(lv.pszText, g_Cat[i].getInfo().c_str());
		ListView_SetItem(g_Categories, &lv);

		lv.iSubItem = 2;
		_itow(g_Cat[i].getNumber(), lv.pszText, 10);
		ListView_SetItem(g_Categories, &lv);
	}

	//set label all of number
	g_Number = CreateWindowEx(0, L"STATIC", L"Tổng: 0 VND", WS_CHILD | WS_VISIBLE | SS_RIGHT,
		20, 240, 550, 24, g_ListCategory, NULL, hInst, NULL);
	wstring t = L"Tổng: " + to_wstring(total) + L" VND";
	SetWindowText(g_Number, t.c_str()); //set total number
	SendMessage(g_Number, WM_SETFONT, WPARAM(g_Font), TRUE); //set font

	//add pie-chart control
	g_pieChart = CreateWindowEx(0, L"Button", L"Biểu đồ", WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
		10, 375, 600, 200, hWnd, NULL, hInst, NULL);
	SendMessage(g_pieChart, WM_SETFONT, WPARAM(g_Font), TRUE); //set font
	setDefaultypeCate(); //set default type for chart
	updateCoordCate(); //update coord part of 6 types.
	return true;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		saveFile(g_Cat, "data.txt");
		PostQuitMessage(0);
		break;
	}

}

void OnDestroy(HWND hWnd)
{
	saveFile(g_Cat, "data.txt");
	PostQuitMessage(0);
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc;
	hdc = BeginPaint(hWnd, &ps);
	drawChart(hdc);
	drawNote(hdc, g_Font);
	EndPaint(hWnd, &ps);
}

void OnCategoryCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDC_ADDBUTTON:
	{
		LRESULT wSelection = SendMessage(g_ListCategory, CB_GETCURSEL, 0, 0);
		wstring error = L"", type = L"", info = L"";
		Category cat;
		WCHAR* buffer = NULL;
		int buf_size = 0;

		//get number 
		buf_size = GetWindowTextLength(g_hNumber);
		buffer = new WCHAR[buf_size + 1];
		GetWindowText(g_hNumber, buffer, buf_size + 1);
		int number = (buf_size != 0) ? _wtoi(buffer) : INT_MIN;
		delete[] buffer;

		//get selected
		if (wSelection >= 0)
		{
			buf_size = SendMessage(g_ListCategory, CB_GETLBTEXTLEN, wSelection, 0);
			buffer = new WCHAR[buf_size + 1];
			SendMessage(g_ListCategory, CB_GETLBTEXT, wSelection, (LPARAM)buffer);
			type = (wstring)buffer;
			delete[] buffer;
		}

		//get detail category
		buf_size = GetWindowTextLength(g_Info);
		buffer = new WCHAR[buf_size + 1];
		GetWindowText(g_Info, buffer, buf_size + 1);
		info = (wstring)buffer;
		delete[] buffer;


		if (wSelection < 0)
		{
			error += L"Bạn chưa chọn loại chi tiêu!!!\n";
		}
		if (info.length() == 0)
		{
			error += L"Bạn chưa nhập chi tiết chi tiêu!!!\n";
		}

		if (number <= 0)
		{
			error += L"Số tiền bạn nhập không hợp lệ!!!";
		}

		if (error != L"")
		{
			MessageBox(hWnd, error.c_str(), L"Lỗi", MB_OK | MB_ICONERROR);
		}
		else
		{
			total += number;
			wstring t = L"Tổng: " + to_wstring(total) + L" VND";
			SetWindowText(g_Number, t.c_str());
			SendMessage(g_Number, WM_SETFONT, WPARAM(g_Font), TRUE);

			cat.setType(type);
			cat.setInfo(info);
			cat.setNumber(number);

			addCategory(cat);
			g_Cat.push_back(cat);
			SendMessage(g_ListCategory, CB_SETCURSEL, -1, 0);
			SetWindowText(g_Info, L"");
			SetWindowText(g_hNumber, L"");

			updateCoordCate();
			InvalidateRect(g_hWnd, NULL, TRUE);
		}
		break;
	}
	}
}

LRESULT CALLBACK categoryWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		HANDLE_MSG(hWnd, WM_COMMAND, OnCategoryCommand);
	}
	return CallWindowProc(wndProcCategory, hWnd, message, wParam, lParam);
}


float getSweepAngle(int index)
{
	int sz = g_Cat.size();
	if (index < 0 || index >= sz)
	{
		return 0;
	}
	float percent = 1.0 * g_Cat[index].getNumber() / total;
	return 1.0 * percent * 360;
}

void updateCoordCate()
{
	typeCate[0].startAngle = 90;
	for (int i = 0; i < 6; i++)
	{
		typeCate[i].number = getNumberofType(defaultType[i]);
		typeCate[i].sweepAngle = 360.0 * typeCate[i].number / total;
		if (i > 0)
		{
			typeCate[i].startAngle = typeCate[i - 1].sweepAngle + typeCate[i - 1].startAngle;
		}
	}
}


//draw chart
void drawChart(HDC hDC)
{
	for (int i = 0; i < 6; i++)
	{
		if (typeCate[i].number != 0)
		{
			SetDCBrushColor(hDC, RGB(typeCate[i].color.red, typeCate[i].color.green, typeCate[i].color.blue));
			SetDCPenColor(hDC, RGB(241, 241, 241));
			SelectObject(hDC, GetStockObject(DC_BRUSH));
			SelectObject(hDC, GetStockObject(DC_PEN));

			BeginPath(hDC);
			MoveToEx(hDC, g_Origin.x, g_Origin.y, NULL);
			AngleArc(hDC, g_Origin.x, g_Origin.y, radius, typeCate[i].startAngle, typeCate[i].sweepAngle);
			LineTo(hDC, g_Origin.x, g_Origin.y);
			EndPath(hDC);
			StrokeAndFillPath(hDC);
		}
	}
}

void drawNote(HDC hDC, HFONT hFont)
{
	SetBkMode(hDC, TRANSPARENT);
	SelectObject(hDC, hFont);
	SetTextColor(hDC, RGB(255, 255, 255));

	vector<Coord> coordNotes = {
		{ 30, 400 }, { 130, 400 }, { 230, 400 },
		{ 30, 500 }, { 130, 500 }, { 230, 500 }
	};

	for (int i = 0; i < typeCate.size(); i++)
	{
		SetDCBrushColor(hDC, RGB(typeCate[i].color.red, typeCate[i].color.green, typeCate[i].color.blue));
		Rectangle(hDC, coordNotes[i].x, coordNotes[i].y, coordNotes[i].x + 50, coordNotes[i].y + 50);
		RECT rect = { coordNotes[i].x, coordNotes[i].y, coordNotes[i].x + 50, coordNotes[i].y + 50 };
		int percent = (int)(100.0*typeCate[i].number / total);
		DrawText(hDC, (to_wstring(percent) + L'%').c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_CENTER | DT_VCENTER);
	}

	SetTextColor(hDC, RGB(0, 0, 0));
	for (int i = 0; i < typeCate.size(); ++i)
	{
		RECT rect = { coordNotes[i].x, coordNotes[i].y + 70, coordNotes[i].x + 120, coordNotes[i].y + 50 };
		DrawText(hDC, typeCate[i].type.c_str(), -1, &rect, DT_SINGLELINE | DT_NOCLIP | DT_LEFT | DT_VCENTER);
	}
}

//category
void addCategory(Category cat)
{
	LVITEM lv;

	lv.mask = LVIF_TEXT;
	lv.iItem = g_Cat.size() - 1;
	lv.iSubItem = 0;
	lv.pszText = new WCHAR[cat.getType().length() + 1];
	wcscpy(lv.pszText, cat.getType().c_str());
	ListView_InsertItem(g_Categories, &lv);

	lv.iSubItem = 1;
	lv.pszText = new WCHAR[cat.getInfo().length() + 1];
	wcscpy(lv.pszText, cat.getInfo().c_str());
	ListView_SetItem(g_Categories, &lv);

	lv.iSubItem = 2;
	_itow(cat.getNumber(), lv.pszText, 10);
	ListView_SetItem(g_Categories, &lv);

}

int getNumberofType(wstring type)
{
	int res = 0;
	for (int i = 0; i < g_Cat.size(); i++)
	{
		if (g_Cat[i].getType() == type)
		{
			res += g_Cat[i].getNumber();
		}
	}
	return res;
}

void setDefaultypeCate()
{
	for (int i = 0; i < 6; i++)
	{
		typeCategory t;
		t.type = defaultType[i];
		typeCate.push_back(t);
	}

	typeCate[0].color = Color(231, 76, 60);
	typeCate[1].color = Color(52, 152, 21);
	typeCate[2].color = Color(39, 174, 96);
	typeCate[3].color = Color(142, 68, 173);
	typeCate[4].color = Color(211, 84, 0);
	typeCate[5].color = Color(44, 62, 80);
}

//file
vector<Category> readFile(string filename)
{
	vector<Category> result;
	locale loc(locale(), new codecvt_utf8<wchar_t>);
	wifstream ifs(filename);
	ifs.imbue(loc);

	if (ifs.is_open())
	{
		while (!ifs.eof())
		{
			wstring buf, data;
			getline(ifs, buf);

			if (buf[0] == 65279)
			{
				buf.erase(0, 1);
			}
			if (buf.size() != 0)
			{
				wstringstream ss(buf);
				vector<wstring> cat;

				while (getline(ss, data, L'-'))
				{
					cat.push_back(data);
				}
				Category cate(cat[0], cat[1], stoi(cat[2]));
				result.push_back(cate);
				total += stoi(cat[2]);
			}
			else
			{
				break;
			}
		}
	}

	ifs.close();

	return result;
}

void saveFile(vector<Category> v, string filename)
{
	locale loc(locale(), new codecvt_utf8<wchar_t>);
	wofstream ofs(filename);
	ofs.imbue(loc);

	for (int i = 0; i < v.size(); ++i)
	{
		wstring buf = v[i].getType() + L'-' + v[i].getInfo() + L'-' + to_wstring(v[i].getNumber());
		ofs << buf;
		if (i < v.size() - 1)
		{
			ofs << endl;
		}
	}
	ofs.close();
}