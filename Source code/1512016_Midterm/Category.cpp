#include "stdafx.h"
#include "Category.h"


Category::Category()
{
	type = L"";
	info = L"";
	number = 0;
}

Category::Category(wstring Type, wstring Info, int Num)
{
	type = Type;
	info = Info;
	number = Num;
}

Category::~Category()
{
}

wstring Category::getType()
{
	return type;
}

wstring Category::getInfo()
{
	return info;
}

int Category::getNumber()
{
	return number;
}

void Category::setType(wstring Type)
{
	type = Type;
}

void Category::setInfo(wstring Info)
{
	info = Info;
}

void Category::setNumber(int Number)
{
	number = Number;
}