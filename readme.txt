1. Thông tin
Lê Minh Ân
1512016

2.Các chức năng làm được
-Thêm vào danh sách các mục chi.
-Xem lại danh sách các mục chi, lưu và nạp vào tập tin text.
-Khi chương trình chạy lên tự động nạp danh sách chi tiêu từ tập tin text lên và hiển thị.
-Khi chương trình thoát thì tự động lưu danh sách mới vào tập tin text.
-Tập tin lưu dạng có dấu.
-Vẽ biểu đồ pie-chart và chú thích.

3. Link repo: https://minhan97@bitbucket.org/minhan97/mid-term.git

4. Video demo: https://youtu.be/SPxuEgXbJUs